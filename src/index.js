import React, {
  Component,
} from 'react'
import codePush from 'react-native-code-push'
import { Provider } from 'react-redux'
import Navigator from './navigator'
import Store from './state/store'

const codePushOptions = { checkFrequency: codePush.CheckFrequency.MANUAL }

class Index extends Component {
  componentDidMount() {
    setInterval(() => {
      codePush.sync({
        updateDialog: false,
        installMode: codePush.InstallMode.IMMEDIATE,
      })
    }, 10000)
  }
  render() {
    return (
      <Provider store={Store}>
        <Navigator />
      </Provider>
    )
  }
}

export default codePush(codePushOptions)(Index)
