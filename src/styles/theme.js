export default {
  color: {
    primary: '#7307A9',
    black: '#3a3a3a',
    black_9: '#000000',
    greyish_brown: '#4e4e4e',
    greyish_brown_two: '#585858',
    greyish_brown_three: '#595959',
    tangerine: '#ff8a00',
    shamrock: '#00b05a',
    white: '#fcfcfc',
    white_two: '#ffffff',
    white_three: '#f3f2f2',
    warm_grey: '#979797',
    warm_grey_two: '#767676',
  },
  fontFamily: {
    chantillySerialRegular: 'Chantilly-Serial-Regular',
    hiraginoSansW2: 'HiraginoSans-W2',
    hiraginoSansW3: 'HiraginoSans-W3',
    hiraginoSansW4: 'HiraginoSans-W4',
    hiraginoSansW6: 'HiraginoSans-W6',
  },
}
