import React from 'react'
import {
  TextInput,
  View,
} from 'react-native'
import PropTypes from 'prop-types'
import {
  css,
  withStyles,
} from '../styles/medPlus'

// import BottomView from '../ui/BottomView'

class Home extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      text: '',
    }
  }

  render() {
    return (
      <View
        {...css(this.props.styles.top)}
      >
        <View
          {...css(this.props.styles.inputContainer)}
        >
          <TextInput
            {...css(this.props.styles.input)}
            placeholder="Type here to translate!"
            onChangeText={text => this.setState({ text })}
            value={this.state.text}
          />
        </View>
      </View>
    )
  }

}

Home.propTypes = {
  /* eslint-disable react/no-unused-prop-types */
  navigation: PropTypes.object.isRequired,
  /* eslint-enable react/no-unused-prop-types */
  styles: PropTypes.object.isRequired,
}

export default withStyles(({ color }) => ({
  top: {
    height: '25%',
    backgroundColor: color.primary,
  },
  input: {
    height: 20,
    borderWidth: 0,
  },
  inputContainer: {
    margin: 20,
    backgroundColor: color.white_two,
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
}))(Home)
