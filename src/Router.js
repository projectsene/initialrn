import {
  Animated,
  Easing,
  Platform,
} from 'react-native'

import theme from './styles/theme'

import home from './containers/Home'

// RouteConfigs
export const Router = {
  home: {
    screen: home,
    navigationOptions: {
      header: null,
    },
  },
}

// StackNavigatorConfig
export const Config = {
  initialRouteName: 'home',
  // initialRouteParams,
  navigationOptions: {
    headerTruncatedBackTitle: 'voltar',
    headerStyle: {
      backgroundColor: theme.color.primary,
      elevation: 0,
      height: Platform.OS === 'ios' ? 70 : 50,
      shadowColor: 'transparent',
      shadowRadius: 0,
      shadowOffset: {
        height: 0,
      },
    },
    headerTintColor: theme.color.white_two,
    headerTitleStyle: {
      // fontFamily: theme.fontFamily.chantillySerialRegular,
      fontSize: 20,
      letterSpacing: 0.24,
      alignSelf: 'center',
    },
  },
  cardStyle: {
    backgroundColor: theme.color.white_two,
  },
  transitionConfig: () => ({
    transitionSpec: {
      duration: 1,
      timing: Animated.timing,
      easing: Easing.step0,
    },
  }),
  // paths,
}
